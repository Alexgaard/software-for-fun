#include <stdio.h>
#include <assert.h>

#define BLOCKALLOCATOR_IMPLEMENTATION
#define BLOCKALLOCATOR_USE_SHORTNAMES
#include "blockAllocator.h"

typedef struct {
	int x;
	int y;
}pos;

void
pos_print(pos* _pos)
{
	printf("pos (%d,%d) at ptr(%p)\n", _pos->x, _pos->y, _pos);
}

void
test_custom_struct(void)
{
	blockAllocator_s pos_allocator = {0};
	blockAllocator_init(&pos_allocator, sizeof(pos), 20);

	printf("total blocks = (%d)\n", pos_allocator.block_count);
	printf("free blocks after alloc = (%d)\n", blockAllocator_count_available_blocks(&pos_allocator));

	pos* pos1 = (pos*)blockAllocator_block_take(&pos_allocator, 1);
	pos* pos2 = (pos*)blockAllocator_block_take(&pos_allocator, 1);

	printf("free blocks after 2 blocks taken = (%d)\n", blockAllocator_count_available_blocks(&pos_allocator));

	pos1->x = 5;
	pos1->y = 10;

	pos2->x = 2;
	pos2->y = 0;

	pos_print(pos1);
	pos_print(pos2);

	blockAllocator_block_return(&pos_allocator, pos2);

	printf("free blocks after 1 returned = (%d)\n", blockAllocator_count_available_blocks(&pos_allocator));

	pos* pos_new = (pos*)blockAllocator_block_take(&pos_allocator, 1);
	blockAllocator_block_clear(&pos_allocator, pos_new);

	printf("free blocks after 1 more taken = (%d)\n", blockAllocator_count_available_blocks(&pos_allocator));
	pos_print(pos_new);

	blockAllocator_block_return_all(&pos_allocator);
	printf("free blocks after return_all() = (%d)\n", blockAllocator_count_available_blocks(&pos_allocator));

	blockAllocator_destroy(&pos_allocator);

}


void
test_sub_block_allocator(void)
{
	uint8_t* sub_array_mem = NULL;
	uint32_t* sub_freetable_mem = NULL;
	blockAllocator_s master_allocator = {0};
	blockAllocator_s sub_allocator = {0};

	blockAllocator_init(&master_allocator, 10, 2);
	printf("available blocks master = (%d)\n", blockAllocator_count_available_blocks(&master_allocator));

	sub_freetable_mem = blockAllocator_block_take(&master_allocator, 1);
	sub_array_mem = blockAllocator_block_take(&master_allocator, 1);

	assert(sub_freetable_mem != NULL);
	assert(sub_array_mem != NULL);

	blockAllocator_set(&sub_allocator,
	                   2,
	                   10,
	                   sub_freetable_mem,
	                   sub_array_mem);

	printf("available blocks sub = (%d)\n", blockAllocator_count_available_blocks(&sub_allocator));
}

void
test_block_debugger(void)
{
	uint8_t* p;
	uint8_t* found_p;
	uint32_t uid = 1;
	blockAllocator_s allocator = {0};

	blockAllocator_init(&allocator, 8, 10);

	(void)blockAllocator_block_take(&allocator, uid++);
	p = blockAllocator_block_take(&allocator, uid++);
	(void)blockAllocator_block_take(&allocator, uid++);


	found_p = blockAllocator_find_uid(&allocator, 2);
	printf("pointer with uid 2 (%p = %p)\n", p, found_p);

	blockAllocator_block_return(&allocator, p);

	blockAllocator_debug_status(&allocator, stdout);
}



int main(int argc, char **argv) {
	(void)argc;
	(void)argv;

	test_custom_struct();
	test_sub_block_allocator();
	test_block_debugger();
	return 0;
}
