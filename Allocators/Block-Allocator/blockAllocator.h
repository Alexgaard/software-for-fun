/* 
# BLOCKALLOCATOR
	Simple BLock Allocator written in ansi-c99.

## ORIGINAL AUTHOR

Thomas Alexgaard Jensen (![gitlab/Alexgaard](https://gitlab.com/Alexgaard))

```txt
   __________________________
  :##########################:
  *##########################*
           .:######:.
          .:########:.
         .:##########:.
        .:##^:####:^##:.
       .:##  :####:  ##:.
      .:##   :####:   ##:.
     .:##    :####:    ##:.
    .:##     :####:     ##:.
   .:##      :####:      ##:.
  .:#/      .:####:.      \#:.

```

## LICENSE

Copyright (c) Thomas Alexgaard Jensen
This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from
the use of this software.
Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software in
   a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

For more information, please refer to
![Zlib License](https://choosealicense.com/licenses/zlib/)

## CHANGELOG

- [0.1] Implemented Block Allocator core functionality.
        Wrote function descriptions.
- [0.0] Initialized library.

## DOCUMENTATION

TODO: Make the freelist bit-sized instead of byte-sized. small memory
      optimization.
*/

#ifdef __cplusplus
extern "C" {
#endif

#ifndef BLOCKALLOCATOR_H
#define BLOCKALLOCATOR_H

#ifndef BLOCKALLOCATOR_API 
#define BLOCKALLOCATOR_API static inline
#endif /*BLOCKALLOCATOR_API */

#include "stdint.h"

typedef struct {
    /*Book-Keeping*/
	uint32_t taken_current;
	uint32_t taken_total;

    /*Mutex*/
    uint8_t is_in_use;

	uint32_t block_size;
	uint32_t block_count;
	uint8_t* memory;
	uint8_t* free_table;
}blockAllocator_s;

BLOCKALLOCATOR_API
uint32_t
blockAllocator_calculate_optimal_size(uint32_t _block_size,
                                      uint32_t _block_count,
                                      uint8_t _alignment);

BLOCKALLOCATOR_API
char
blockAllocator_set(blockAllocator_s* _ba,
                              uint32_t _block_size,
                              uint32_t _block_count,
                              uint32_t* _uid_table_data,
                              uint8_t* _data_data);

BLOCKALLOCATOR_API
char
blockAllocator_init(blockAllocator_s* _ba, int _block_size, int _block_count);


BLOCKALLOCATOR_API
void
blockAllocator_destroy(blockAllocator_s* _ba);

BLOCKALLOCATOR_API
void*
blockAllocator_block_take(blockAllocator_s* _ba, uint32_t _uid);

BLOCKALLOCATOR_API
void*
blockAllocator_block_return(blockAllocator_s* _ba, void* _block);

BLOCKALLOCATOR_API
void
blockAllocator_block_return_all(blockAllocator_s* _ba);

BLOCKALLOCATOR_API
void*
blockAllocator_find_uid(blockAllocator_s* _ba, uint32_t _uid);

BLOCKALLOCATOR_API
int32_t
_blockAllocator_find_block_index_by_ptr(blockAllocator_s* _ba, void* _block);

BLOCKALLOCATOR_API
void*
blockAllocator_block_clear(blockAllocator_s* _ba, void* _block);

BLOCKALLOCATOR_API
int
blockAllocator_count_available_blocks(blockAllocator_s* _ba);

BLOCKALLOCATOR_API
void
blockAllocator_debug_status(blockAllocator_s* _ba, FILE* _out);

/******************************************************************************/
#define BLOCKALLOCATOR_IMPLEMENTATION
#ifdef BLOCKALLOCATOR_IMPLEMENTATION

BLOCKALLOCATOR_API
char
blockAllocator_init(blockAllocator_s* _ba,
                    int _block_size,
                    int _block_count,
                    uint8_t* _memory,
                    uint8_t* _freetable)
/**
 * blockAllocator_set() - Create block allocator without automatic
 *                        allocation of memory.
 *
 * @arg1: Ptr to block allocator.
 * @arg2: size of single block specified in bytes.
 * @arg3: amount blocks to be allocated.
 * @arg4: PtrPtr to memory for freetable.
 * @arg5: PtrPtr to memory for block array.
 *
 * Creates blockallocator without automatic memory allocation.
 *
 * Return: error management code, 1 for error, 0 for clean exit.
 */
{
	if (_ba == NULL || _uid_table == NULL || _data == NULL)
		return 1;
	_ba->_block_size = _block_size;
	_ba->block_count = _block_count;
	_ba->uid_table = _uid_table;
	_ba->data = _data;
	return 0;
}

BLOCKALLOCATOR_API
void*
blockAllocator_block_take(blockAllocator_s* _ba, uint32_t _uid)
/**
 * blockAllocator_block_take() - Take a block from block allocator.
 * @arg1: Ptr to block allocator.
 * @arg2: Identification number for taken block.
 *
 * Takes a block of memory in blockallocator, marks it as used with assigned
 * unique identification (uid).
 *
 * Allowed uid's: any value greater than, or equal to 1.
 * uid is not enforced, blocks can have the same uid, it is used purely for
 * identification purposes, should raw pointers not be wanted.
 *
 * Return: Taken block ptr, NULL on invalid block.
 */
{
	int i;
	if (_ba == NULL || _uid < 1)
		return NULL;

	for (i = 0; i < _ba->block_count; i++) {
		if (_ba->uid_table[i] == BLOCKALLOCATOR_ISFREE) {
			_ba->uid_table[i] = _uid;
			return _ba->data + (i * _ba->_block_size);
		}
	}
	return NULL;
}

BLOCKALLOCATOR_API
int
_blockAllocator_find_block_index_by_ptr(blockAllocator_s* _ba, void* _block)
/**
 * _blockAllocator_find_block_index_by_ptr() - Find index of block.
 * @arg1: Ptr to block allocator.
 * @arg2: Ptr to block to find.
 *
 * INTERNAL FUNCTION
 *
 * Find index of block in block allocator, if exists.
 *
 * Return: block idx, -1 on invalid block.
 */
{
	int i;
	if (_ba == NULL || _block == NULL)
		return -1;
	for (i = 0; i < _ba->block_count; i++)
		if (_ba->data + (i * _ba->_block_size) == _block)
			return i;
	return -1;
}

BLOCKALLOCATOR_API
void*
blockAllocator_block_return(blockAllocator_s* _ba, void* _block)
/**
 * blockAllocator_block_return() - Return block to allocator.
 * @arg1: Ptr to block allocator.
 * @arg2: Ptr to block.
 *
 * Return block in block allocator if exists, Returned blocks are treated as
 * reuseable and can be taken by
 *
 *     blockAllocator_block_take()
 *
 * Returned block memory is NOT cleared.
 *
 * Return: NULL.
 */
{
	int block_idx = -1;
	if (_ba == NULL || _block == NULL)
		return NULL;
	block_idx = _blockAllocator_find_block_index_by_ptr(_ba, _block);
	if (block_idx > 0)
		_ba->uid_table[block_idx] = BLOCKALLOCATOR_ISFREE;
	return NULL;
}

BLOCKALLOCATOR_API
void
blockAllocator_block_return_all(blockAllocator_s* _ba)
/**
 * blockAllocator_block_return_all() - Return block to allocator.
 * @arg1: Ptr to block allocator.
 *
 * Return all block in block allocator. Returned blocks are treated as
 * reuseable and can be taken by
 *
 *     blockAllocator_block_take()
 *
 * Returned block memory is NOT cleared.
 */
{
	int i;
	if (_ba == NULL)
		return;
	for (i = 0; i < _ba->block_count; i++)
		_ba->uid_table[i] = BLOCKALLOCATOR_ISFREE;
}

BLOCKALLOCATOR_API
void*
blockAllocator_find_uid(blockAllocator_s* _ba, uint32_t _uid)
/**
 * blockAllocator_find_uid() - find block by uid.
 * @arg1: Ptr to block allocator.
 * @arg2: uid to search for.
 *
 * Find block memory with specified uid.
 * 
 *
 * Return: ptr to memory.
 */
{
	int i;
	if (_ba == NULL || _uid < 1)
		return NULL;
	for (i = 0; i < _ba->block_count; i++)
		if (_ba->uid_table[i] == _uid)
			return _ba->data + (i * _ba->_block_size);
	return NULL;
}

BLOCKALLOCATOR_API
void*
blockAllocator_block_clear(blockAllocator_s* _ba, void* _block)
/**
 * blockAllocator_block_clear() - Clear memory of block.
 * @arg1: Ptr to block allocator.
 * @arg2: Ptr to block.
 *
 * Clear block memory in block allocator if exists.
 *
 * Return: @arg1.
 */
{
	int block_idx;
	int i;
	if (_ba == NULL || _block == NULL)
		return NULL;
	block_idx = _blockAllocator_find_block_index_by_ptr(_ba, _block);
	if (block_idx < 0)
		return NULL;

	/*Clear blockmemory*/
	for (i = 0; i < _ba->_block_size; i++)
		_ba->data[i * _ba->_block_size] = 0;
	return _block;
}

BLOCKALLOCATOR_API
int
blockAllocator_count_available_blocks(blockAllocator_s* _ba)
/**
 * blockAllocator_block_count() - count free blocks.
 * @arg1: Ptr to block allocator.
 *
 * Counts amount of blocks not marked as taken.
 *
 * Return: free block count.
 */
{
	int n = 0;
	int i;

	if (_ba == NULL)
		return -1;
	for (i = 0; i < _ba->block_count; i++)
		if (_ba->uid_table[i] == BLOCKALLOCATOR_ISFREE)
			n++;
	return n;
}

BLOCKALLOCATOR_API
void
blockAllocator_debug_status(blockAllocator_s* _ba, FILE* _out)
/**
 * blockAllocator_debug_status() - print debugging status of a blockallocator.
 * @arg1: Ptr to block allocator.
 * @arg2: Stream to print to.
 *
 * Prints Debug information for blockallocator.
 */
{
	int i;
	int available_blocks = blockAllocator_count_available_blocks(_ba);
	fprintf(_out, 
	"blockAllocator_debug_status():\n"
	"block size/count (%d/%d)\n"
	"Total block memory size (%d)\n"
	"block memory ptr (%p)\n"
	"freetable memory ptr (%p)\n"
	"available block count (%d)\n",
	_ba->_block_size,
	_ba->block_count,
	_ba->_block_size * _ba->block_count,
	_ba->data,
	_ba->uid_table,
	available_blocks);

	fprintf(_out, "Block freetable:\n");
	for (i = 0; i < _ba->block_count; i++) {
		fprintf(_out, "\t%d: ", i);
		if (_ba->uid_table[i] == BLOCKALLOCATOR_ISFREE)
			fprintf(_out, "[0]\n");
		else
			fprintf(_out, "%d\n", _ba->uid_table[i]);
	}
}

#endif /*BLOCKALLOCATOR_IMPLEMENTATION*/
#endif /*BLOCKALLOCATOR_H*/

#ifdef __cplusplus
}
#endif
