/*
SHORT DESCRIPTION:


ORIGINAL AUTHOR:
Thomas Alexgaard Jensen (https://gitlab.com/Alexgaard)

   __________________________
  :##########################:
  *##########################*
           .:######:.
          .:########:.
         .:##########:.
        .:##^:####:^##:.
       .:##  :####:  ##:.
      .:##   :####:   ##:.
     .:##    :####:    ##:.
    .:##     :####:     ##:.
   .:##      :####:      ##:.
  .:#/      .:####:.      \#:.


LICENSE:
Copyright (c) <Thomas Alexgaard Jensen>
This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from
the use of this software.
Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software in
   a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

For more information, please refer to
<https://choosealicense.com/licenses/zlib/>

CHANGELOG:
[1.0] Added core functionality.
[0.0] Initialized library.
*/

#ifndef CTUPLE_H
#define CTUPLE_H

#include <stdint.h> /*uint32_t + uint8_t*/
#include <stdio.h>  /*NULL*/

#ifndef CTUPLE_NO_STDLIB
    #include <stdlib.h> /*malloc + realloc + free*/
    #define CTUPLE_MALLOC  malloc
    #define CTUPLE_FREE    free
#endif

#ifndef CTUPLE_API
#define CTUPLE_API static inline
#endif

#define CTUPLE_BACKEND static

/*
  tuple memory layout:

  [count] [data*]

  data* = [offsets] [tuple entries]

  Tuple of size 3 (int char int) memory layout:
  ctuple_new(3, sizeof(int), sizeof(char), sizeof(int))

  (count = 3, data* =
    [offset1 (value=0)]
    [offset2 (value=4)]
    [offset3 (value=5)]
    [data1 (4 bytes)]
    [data2 (1 byte)]
    [data3 (4 bytes)]
*/

#define ctuple_def(...) ctuple

#define _ctuple_offset(TUPLE, INDEX)                                           \
	( (INDEX > 0 && INDEX < TUPLE.count) ? TUPLE.data[INDEX] : 0 )

#define ctuple(TUPLE, INDEX, TYPE)                                             \
	TUPLE.data[_ctuple_offset(TUPLE, INDEX)]

typedef struct {
	uint8_t count;
	uint32_t* data;
}ctuple;

CTUPLE_API
ctuple
ctuple_new(int _n, ...);

CTUPLE_API
void
ctuple_destroy(ctuple _tuple);

/******************************************************************************/
#ifdef CTUPLE_IMPLEMENTATION

CTUPLE_API
ctuple
ctuple_new(int _n, ...)
{
	int i;
	int memsize = 0;
	ctuple out = {0};
	va_list ap;
	va_list bp;

	va_copy(bp, ap);
	va_start(ap, _n);
	out.count = _n;
	for (i = 0; i < _n; i++)
		memsize += sizeof(uint32_t) + va_arg(ap, size_t);
	out.data = CTUPLE_MALLOC(memsize);
	va_end(ap);

	int offsets = 0;
	va_start(bp, _n);
	for (i = 0; i < _n; i++) {
		out.data[i] = offsets;
		offsets += va_arg(bp, size_t);
	}
	va_end(bp);
	return out;
}

CTUPLE_API
void
ctuple_destroy(ctuple _tuple)
{
	if (_tuple.data != NULL)
		CTUPLE_FREE(_tuple.data);
	_tuple.data = NULL;
}

#endif /*CTUPLE_IMPLEMENTATION*/
#endif /*CTUPLE_H*/
