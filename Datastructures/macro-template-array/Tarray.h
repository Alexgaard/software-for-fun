#ifndef TARRAY_H
#define TARRAY_H

#include <stdint.h> /*uint32_t + uint8_t*/

#ifndef TARRAY_NO_STDLIB
#include <stdlib.h> /*malloc + realloc + free*/
#define TARRAY_MALLOC(n)     malloc(n)
#define TARRAY_REALLOC(p, n) realloc(p,n)
#define TARRAY_FREE(p)       free(p)
#endif

#ifndef TARRAY_GROWTH
#define TARRAY_GROWTH 3.0f
#endif /*TARRAY_GROWTH*/

typedef struct {
	uint32_t size;
	uint32_t max;
	uint32_t it;
}_Tarray_header_s;

#define Tarray(T) T*

#define Tarray_free(ARR)       ( (ARR) ? TARRAY_FREE(Tarray_header(ARR)) : 0 )
#define Tarray_header(ARR)     ( (ARR) ? (((_Tarray_header_s*)(ARR-1))-1) : 0 )
#define Tarray_len(ARR)        ( (ARR) ? Tarray_header(ARR)->size : 0 )
#define Tarray_maxlen(ARR)     ( (ARR) ? Tarray_header(ARR)->max : 0)
#define Tarray_last(ARR)       ( (ARR) ? ARR[Tarray_len(ARR)] : 0 )
#define Tarray_clear(ARR)      ( (ARR) ? Tarray_header(ARR)->size = 0 : 0 )
#define Tarray_resize(ARR, N)  ( (ARR) = _Tarray_resize(ARR, sizeof(*ARR), N) )

#define Tarray_shrink_fit(ARR)                                                 \
	( (ARR) ? Tarray_resize(ARR, Tarray_len(ARR)) : 0 )

#define Tarray_swap(ARR, i, j)                                                 \
	( (!ARR) ?                                                                 \
	0 :                                                                        \
	  (ARR[-1] = ARR[i]),                                                      \
	  (ARR[i] = ARR[j]),                                                       \
	  (ARR[j] = ARR[-1]) )

#define Tarray_grow(ARR)                                                       \
	( (!ARR) ?                                                                 \
	Tarray_resize(ARR, 4) :                                                    \
	Tarray_resize(ARR, 1 + (Tarray_len(ARR) * TARRAY_GROWTH)) )

#define Tarray_maybe_grow(ARR)                                                 \
	( (!(ARR) || Tarray_len(ARR) >= Tarray_maxlen(ARR)) ?                      \
	  Tarray_grow(ARR) : 0 )

#define Tarray_maybe_grow(ARR)                                                 \
	( (!(ARR) || Tarray_len(ARR) >= Tarray_maxlen(ARR)) ?                      \
	  Tarray_grow(ARR) : 0 )

#define Tarray_push(ARR, ELEM)                                                 \
	(Tarray_maybe_grow(ARR), ARR[Tarray_header(ARR)->size++] = (ELEM))

#define Tarray_pop(ARR)                                                        \
	( (Tarray_len(ARR) < 1) ? 0 : ARR[Tarray_header(ARR)->size--] )

#define Tarray_remfirst(ARR)                                                   \
	do {                                                                       \
		Tarray_header(ARR)->it = 1;                                            \
	do {                                                                       \
	Tarray_swap(ARR, Tarray_header(ARR)->it, Tarray_header(ARR)->it-1 );       \
	Tarray_header(ARR)->it++;                                                  \
	} while (Tarray_header(ARR)->it < Tarray_len(ARR));                        \
	Tarray_header(ARR)->size--;                                                \
	} while (0)

#define Tarray_dequeue(ARR, OUT)                                               \
	( OUT = ARR[0], Tarray_remfirst(ARR) )

#define Tarray_foreach(MEMBER, ARR)                                            \
	for (Tarray_header(ARR)->it = 0, MEMBER = ARR[Tarray_header(ARR)->it];     \
		 Tarray_header(ARR)->it < Tarray_len(ARR);                             \
		 Tarray_header(ARR)->it++, MEMBER = ARR[Tarray_header(ARR)->it])

#ifdef __cplusplus
template<class T>
T*
_Tarray_resize(T* _arr, uint32_t _sizeof_arr_member, uint32_t _n);
#else /*NOT__cplusplus*/
void*
_Tarray_resize(void* _arr, uint32_t _sizeof_arr_member, uint32_t _n);
#endif /*NOT__cplusplus*/

/******************************************************************************/
#ifdef TARRAY_IMPLEMENTATION
#ifdef __cplusplus
template<class T>
T*
_Tarray_resize(T* _arr, uint32_t _sizeof_arr_member, uint32_t _n)
{
	uint32_t mem;
	if (_sizeof_arr_member == 0 || _n == 0)
		return NULL;

	mem = sizeof(_Tarray_header_s) + (_sizeof_arr_member * _n);
	if (_arr == NULL) {
		/*Allocate new array*/
		_arr = reinterpret_cast<T*>(TARRAY_MALLOC(mem));
		if (_arr == NULL)
			return NULL;
		_arr += sizeof(_Tarray_header_s);
		Tarray_header(_arr)->size = 0;
		Tarray_header(_arr)->max  = _n;
		return _arr;
	}
	/*Resize existing array*/
	_arr = reinterpret_cast<T*>(TARRAY_REALLOC(Tarray_header(_arr), mem));
	if (_arr == NULL)
		return NULL;
	_arr += sizeof(_Tarray_header_s);
	Tarray_header(_arr)->max = _n;
	return _arr;
}
#else /*NOT__cplusplus*/
void*
_Tarray_resize(void* _arr, uint32_t _sizeof_arr_member, uint32_t _n)
{
	uint32_t mem;
	if (_sizeof_arr_member == 0 || _n == 0)
		return NULL;

	mem = sizeof(_Tarray_header_s) + (_sizeof_arr_member * _n);
	if (_arr == NULL) {
		/*Allocate new array*/
		_arr = TARRAY_MALLOC(mem);
		if (_arr == NULL)
			return NULL;
		_arr += sizeof(_Tarray_header_s) + _sizeof_arr_member;
		Tarray_header(_arr)->size = 0;
		Tarray_header(_arr)->max  = _n;
		return _arr;
	}
	/*Resize existing array*/
	_arr = TARRAY_REALLOC(Tarray_header(_arr), mem);
	if (_arr == NULL)
		return NULL;
	_arr += sizeof(_Tarray_header_s) + _sizeof_arr_member;
	Tarray_header(_arr)->max = _n;
	return _arr;
}
#endif /*NOT__cplusplus*/

#endif /*TARRAY_IMPLEMENTATION*/
#endif /*TARRAY_H*/
