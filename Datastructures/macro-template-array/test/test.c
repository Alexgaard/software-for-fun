#include <bits/stdint-uintn.h>
#include <stdio.h>

#define TARRAY_IMPLEMENTATION
#include "../Tarray.h"

#define ARRPRINT_INT(arr)                                                      \
	do {                                                                       \
	uint32_t i = 0;                                                            \
	printf("[size=%d, max=%d] ", Tarray_len(arr), Tarray_maxlen(arr));         \
	for (i = 0; i < Tarray_len(arr); i++)                                      \
		printf("%d ", arr[i]);                                                 \
	printf("\n");						                                       \
	} while (0)

#define LINE \
	printf("---------------------------------------------------------------\n")

void
test_usage(void)
{
	int i;

	Tarray(int32_t) arr = NULL;
	Tarray_push(arr, 69);
	ARRPRINT_INT(arr);
	Tarray_push(arr, 420);
	ARRPRINT_INT(arr);
	Tarray_push(arr, 255);
	ARRPRINT_INT(arr);
	Tarray_push(arr, 0);
	ARRPRINT_INT(arr);
	Tarray_push(arr, 11);
	ARRPRINT_INT(arr);

	for (i = 0; i < 10; i++) {
		Tarray_push(arr, i);
		ARRPRINT_INT(arr);
	}

	for (i = 0; i < 8; i++) {
		Tarray_pop(arr);
		ARRPRINT_INT(arr);
	}
	uint32_t tmp = 0;
	for (i = 0; i < 2; i++) {
		tmp = arr[0];
		printf("dequeued %d\n",tmp);
		Tarray_remfirst(arr);
		ARRPRINT_INT(arr);
	}
	for (i = 0; i < 2; i++) {
		tmp = arr[0];
		printf("dequeued %d\n",tmp);
		Tarray_remfirst(arr);
		ARRPRINT_INT(arr);
	}

	Tarray_free(arr);
}

void
test_swap(void)
{
	Tarray(int32_t) arr = {0};

	Tarray_push(arr, 69);
	Tarray_push(arr, 420);
	Tarray_push(arr, 255);
	Tarray_push(arr, 0);
	Tarray_push(arr, 11);
	ARRPRINT_INT(arr);

	Tarray_swap(arr, 0, 1);
	ARRPRINT_INT(arr);

	Tarray_swap(arr, 2, 4);
	ARRPRINT_INT(arr);

	Tarray_free(arr);
}
void
test_iterators(void)
{
	uint32_t i;
	int32_t number;
	Tarray(int32_t) numbers = NULL;

	for (i = 0; i < 10; i++)
		Tarray_push(numbers, i);

	for (i = 0; i < Tarray_len(numbers); i++)
		printf("%d ", numbers[i]);
	printf("\n");

	Tarray_foreach(number, numbers)
		printf("%d ", number);
	printf("\n");

	Tarray_free(numbers);
}

int main(int argc, char **argv) {
	(void)argc;
	(void)argv;

#ifdef __cplusplus
	printf("-----------------------------------\n");
	printf("COMPILED IN C++\n");
	printf("-----------------------------------\n");
#else /*not __cplusplus*/ 
	printf("-----------------------------------\n");
	printf("COMPILED IN C99\n");
	printf("-----------------------------------\n");
#endif /*__cplusplus*/

	printf("usage\n");
	test_usage();
	LINE;
	printf("swap\n");
	test_swap();
	LINE;
	printf("iterators\n");
	test_iterators();
	LINE;
	/*
	printf("large scale data\n");
	test_custom_large_data();
	LINE;
	*/
	return 0;
}
