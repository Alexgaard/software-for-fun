/* 
# LIBRARY

## ORIGINAL AUTHOR

Thomas Alexgaard Jensen (![gitlab/Alexgaard](https://gitlab.com/Alexgaard))

```txt
   __________________________
  :##########################:
  *##########################*
           .:######:.
          .:########:.
         .:##########:.
        .:##^:####:^##:.
       .:##  :####:  ##:.
      .:##   :####:   ##:.
     .:##    :####:    ##:.
    .:##     :####:     ##:.
   .:##      :####:      ##:.
  .:#/      .:####:.      \#:.

```

## LICENSE

Copyright (c) Thomas Alexgaard Jensen
This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from
the use of this software.
Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software in
   a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

For more information, please refer to
![Zlib License](https://choosealicense.com/licenses/zlib/)

## CHANGELOG

- [0.1] Implemented core utils for linkedlist.
- [0.0] Initialized library.

## DOCUMENTATION
Base implementation:
https://www.youtube.com/watch?v=VOpjAHCee7c

*/

#ifdef __cplusplus
extern "C" {
#endif

#include "stdio.h"
#include "stdint.h"
#include "stddef.h"
#include "stdlib.h"

#ifndef LINKEDLIST_H
#define LINKEDLIST_H

struct llnode {
	uint32_t size;
	struct llnode* next;
	void* payload;
};
typedef struct llnode llnode_s;

llnode_s*
linkedlist_new(uint32_t _size, void* _payload);

uint32_t
linkedlist_length(llnode_s* _head);

llnode_s*
linkedlist_find_with_size(llnode_s* _head, uint32_t _size);

llnode_s*
linkedlist_find_with_payload(llnode_s* _head, void* _payload);

llnode_s*
linkedlist_find_tail(llnode_s* _head);

llnode_s*
linkedlist_insert_at_head(llnode_s** _head, llnode_s* _new);

llnode_s*
linkedlist_insert_at_tail(llnode_s** _head, llnode_s* _new);

llnode_s*
linkedlist_insert_after_link(llnode_s* _node, llnode_s* _new);

llnode_s*
linkedlist_remove(llnode_s** _head, llnode_s* _node);

llnode_s*
linkedlist_remove_after_link(llnode_s* _node);

llnode_s*
linkedlist_remove_all(llnode_s** _head);

#ifdef LINKEDLIST_DEBUG_IMPLEMENTATION
void
linkedlist_print(llnode_s* _head);
#endif /*LINKEDLIST_DEBUG_IMPLEMENTATION*/

/*****************************************************************************/
#define LINKEDLIST_IMPLEMENTATION
#ifdef LINKEDLIST_IMPLEMENTATION

llnode_s*
linkedlist_new(uint32_t _size, void* _payload)
{
	llnode_s* n = (llnode_s*)malloc(sizeof(llnode_s));
	n->next = NULL;
	n->size = _size;
	n->payload = _payload;
	return n;
}

uint32_t
linkedlist_length(llnode_s* _head)
{
	uint32_t length = 0;
	llnode_s* curr = _head;
	while (curr != NULL) {
		length++;
		curr = curr->next;
	}
	return length;
}

llnode_s*
linkedlist_find_with_size(llnode_s* _head, uint32_t _size)
{
	llnode_s* curr = _head;
	while (curr != NULL) {
		if (curr->size == _size)
			return curr;
		curr = curr->next;
	}
	return NULL;
}

llnode_s*
linkedlist_find_with_payload(llnode_s* _head, void* _payload)
{
	llnode_s* curr = _head;
	if (_head == NULL)
		return NULL;
	while (curr != NULL) {
		if (curr->payload == _payload)
			return curr;
		curr = curr->next;
	}
	return NULL;
}

llnode_s*
linkedlist_insert_at_head(llnode_s** _head, llnode_s* _new)
{
	if (_head == NULL || _new == NULL)
		return NULL;
	_new->next = *_head;
	*_head = _new;
	return _new;
}

llnode_s*
linkedlist_find_tail(llnode_s* _head)
{
	llnode_s* curr = _head;
	while (curr != NULL) {
		if (curr->next == NULL)
			return curr;
		curr = curr->next;
	}
	return NULL;
}

llnode_s*
linkedlist_insert_after_link(llnode_s* _node, llnode_s* _new)
{
	_new->next = _node->next;
	_node->next = _new;
	return _new;
}

llnode_s*
linkedlist_insert_at_tail(llnode_s** _head, llnode_s* _new)
{
	return linkedlist_insert_after_link(linkedlist_find_tail(*_head), _new);
}

llnode_s*
linkedlist_remove_after_link(llnode_s* _node)
{
	llnode_s* remove = _node->next;
	_node->next = remove->next;
	free(remove);
	return _node;
}

llnode_s*
linkedlist_remove(llnode_s** _head, llnode_s* _node)
{
	llnode_s* tmp = NULL;
	/*Check if link to remove is head*/
	if (_node == *_head) {
		tmp = *_head;
		*_head = tmp->next;
		free(tmp);
		return *_head;
	}

	tmp = *_head;
	/*Find link that points to link to remove*/
	while (1) {
		if (tmp->next == _node) {
			linkedlist_remove_after_link(tmp);
			return tmp;
		}
		tmp = tmp->next;
		if (tmp == NULL)
			return NULL;
	}
}

llnode_s*
linkedlist_remove_all(llnode_s** _head)
{
	llnode_s* curr = *_head;
	llnode_s* next;
	while (curr != NULL) {
		next = curr->next;
		free(curr);
		curr = next;
	}
	*_head = NULL;
	return NULL;
}

#ifdef LINKEDLIST_DEBUG_IMPLEMENTATION
void
linkedlist_print(llnode_s* _head)
{
	llnode_s* curr;
	printf("Linked List (ptr=%p)(length=%d)\n",
	       _head, linkedlist_length(_head));
	if (_head == NULL)
		return;
	curr = _head;
	while (1) {
		if (curr == NULL)
			break;
		printf("\tptr=(%p) size=(%d), payload_ptr=(%p)\n", curr, curr->size, curr->payload);
		curr = curr->next;
	}
}
#endif /*LINKEDLIST_DEBUG_IMPLEMENTATION*/

#endif /*LINKEDLIST_IMPLEMENTATION*/
#endif /*LINKEDLIST_H*/

#ifdef __cplusplus
}
#endif
