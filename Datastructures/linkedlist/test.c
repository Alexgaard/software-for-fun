#include <stdio.h>

#define LINKEDLIST_IMPLEMENTATION
#define LINKEDLIST_DEBUG_IMPLEMENTATION
#include "linkedlist.h"

#ifndef DBTARGET 
#define DBTARGET stdout
#endif /*DBTARGET*/

#define dbprint(str, ...) \
	fprintf(DBTARGET, "{%s,%d} " str,__FILE__, __LINE__, ##__VA_ARGS__)

#define dbprintbare(str, ...) \
	fprintf(DBTARGET, str, ##__VA_ARGS__)

#define dbfncall(FNNAME, FNCALL)                                               \
	do {                                                                       \
	dbprintbare("\n-------------------------------------------------------\n");\
	dbprintbare(FNNAME " TEST\n");                                             \
	dbprintbare("---------------\n");                                          \
	FNCALL                                                                     \
	} while (0);

#define dbprint_arr(arr, n, specifier)                                         \
	do {                                                                       \
	dbprint("ptr (%p): (n = %d, memsize = %ld)\n", arr, n, sizeof(*arr) * n);  \
	for (int _i__ = 0; _i__ < arr_size; _i__++)                                \
		fprintf(DBTARGET, specifier " ", arr[_i__]);                           \
	fprintf(DBTARGET, "\n");                                                   \
	} while (0)

char
db_isNULL(void* ptr)
{
	if (ptr == NULL) {
		dbprint("ptr (%p) is invalid\n", ptr);
		return 1;
	}
	return 0;
}

void
linkedlist_manual_test(void) 
{
	llnode_s l1 = {0};
	llnode_s l2 = {0};
	llnode_s l3 = {0};

	l1.size = 25;
	l2.size = 37;
	l3.size = 19;

	l1.next = &l2;
	l2.next = &l3;

	linkedlist_print(&l1);
}

void
linkedlist_new_test(int _n)
{
	int i;
	llnode_s* head = NULL;
	llnode_s* tmp = NULL;

	dbprint("(%d) links specified\n", _n);
	for (i = 0; i < _n; i++) {
		tmp = linkedlist_new(i, NULL);
		if (db_isNULL(tmp))
			break;
		linkedlist_insert_at_head(&head, tmp);
	}

	tmp = linkedlist_new(102, NULL);
	linkedlist_insert_after_link(linkedlist_find_with_size(head, 8), tmp);


	linkedlist_print(head);
	dbprint("length of list = (%d)\n", linkedlist_length(head));
	linkedlist_remove_all(&head);
}

void
linkedlist_add_and_remove_test(void) 
{
	int i;
	llnode_s* head = NULL;
	llnode_s* tmp = NULL;

	for (i = 0; i < 10; i++) {
		tmp = linkedlist_new(i, NULL);
		if (db_isNULL(tmp))
			break;
		linkedlist_insert_at_head(&head, tmp);
	}

	tmp = linkedlist_new(102, NULL);
	linkedlist_insert_after_link(linkedlist_find_with_size(head, 8), tmp);

	linkedlist_remove(&head, linkedlist_find_with_size(head, 4));

	linkedlist_print(head);
	linkedlist_remove_all(&head);
}

void
linkedlist_remove_all_test(void) 
{
	int i;
	llnode_s* head = NULL;
	llnode_s* tmp = NULL;

	for (i = 0; i < 10; i++) {
		tmp = linkedlist_new(i, NULL);
		if (db_isNULL(tmp))
			break;
		linkedlist_insert_at_head(&head, tmp);
	}

	linkedlist_print(head);
	linkedlist_remove_all(&head);
	linkedlist_print(head);
}

void
linkedlist_remove_single_test(void) 
{
	int i;
	llnode_s* head = NULL;
	llnode_s* tmp = NULL;

	for (i = 0; i < 10; i++) {
		tmp = linkedlist_new(i, NULL);
		if (db_isNULL(tmp))
			break;
		linkedlist_insert_at_head(&head, tmp);
	}

	linkedlist_print(head);

	linkedlist_remove(&head, linkedlist_find_with_size(head, 5));

	linkedlist_print(head);
	linkedlist_remove_all(&head);
}

int main(int argc, char **argv) {
	(void)argc;
	(void)argv;
	dbfncall("LINKEDLIST MANUAL", linkedlist_manual_test();)
	dbfncall("LINKEDLIST NEW", linkedlist_new_test(20);)
	dbfncall("LINKEDLIST ADD & REMOVE", linkedlist_add_and_remove_test();)
	dbfncall("LINKEDLIST REMOVE ALL", linkedlist_remove_all_test();)
	dbfncall("LINKEDLIST REMOVE SINGLE", linkedlist_remove_single_test();)
	return 0;
}
