/* 
SHORT DESCRIPTION:
	Pseudo random library, allowing for seeded and "pseudo random" functions 
	written in ansi-c99.

ORIGINAL AUTHOR:
	Thomas Alexgaard Jensen (https://gitlab.com/Alexgaard)

   __________________________
  :##########################:
  *##########################*
           .:######:.
          .:########:.
         .:##########:.
        .:##^:####:^##:.
       .:##  :####:  ##:.
      .:##   :####:   ##:.
     .:##    :####:    ##:.
    .:##     :####:     ##:.
   .:##      :####:      ##:.
  .:#/      .:####:.      \#:. 


LICENSE:
	Copyright (c) <Thomas Alexgaard Jensen>
	This software is provided 'as-is', without any express or implied warranty.
	In no event will the authors be held liable for any damages arising from
	the use of this software.
	Permission is granted to anyone to use this software for any purpose,
	including commercial applications, and to alter it and redistribute it
	freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented; you must not
	   claim that you wrote the original software. If you use this software in
	   a product, an acknowledgment in the product documentation would be
	   appreciated but is not required.

	2. Altered source versions must be plainly marked as such, and must not be
	   misrepresented as being the original software.

	3. This notice may not be removed or altered from any source distribution.

	For more information, please refer to 
	<https://choosealicense.com/licenses/zlib/>

DESCRIPTION:

ALTERNATIVES:

MISSING:

CHANGELOG:
	[1.1] Started implementing defines for types, to ensure compatability.
	[1.0] Re-implemented library, and added more features for seeded values. 
	      Added namespace.
	      Removed magic numbers.
	      Added macros for more user control.
	      Properly converted to header-only-library.
	      Converted to constant-size types.
	[0.0] Initialized library.

DOCUMENTATION:

*/

#ifdef __cplusplus
extern "C" {
#endif

#ifndef PSRAND_H
#define PSRAND_H

#ifndef USE_CUSTOM_PSRAND_AUTO_SEED_FUNCTION
#include "time.h"
#define PSRAND_AUTO_SEED_FUNCTION clock()
#endif /*USE_CUSTOM_PSRAND_AUTO_SEED_FUNCTION*/


#ifndef PSRAND_USE_SPECIFIED_TYPES
#define PSRAND_USE_SPECIFIED_TYPES
#include "stdint.h"
#define PSRAND_INT32U uint32_t 
#define PSRAND_INT32  int32_t 
#define PSRAND_INT8U  uint8_t 
#else /*NOT PSRAND_USE_SPECIFIED_TYPES*/
#define PSRAND_INT32U unsigned int 
#define PSRAND_INT32  int 
#define PSRAND_INT8U  unsigned char 
#endif /*PSRAND_USE_SPECIFIED_TYPES*/

PSRAND_INT32U
psrand_seeded_ulong(PSRAND_INT32U _seed);

PSRAND_INT8U
psrand_seeded_bool(PSRAND_INT32U _seed);

PSRAND_INT32U
psrand_seeded_ubetween(PSRAND_INT32U _min, PSRAND_INT32U _max, PSRAND_INT32U _seed);

PSRAND_INT32U
psrand_random_ulong(void);

PSRAND_INT8U
psrand_random_bool(void);

PSRAND_INT32U
psrand_random_ubetween(PSRAND_INT32U _min, PSRAND_INT32U _max);

PSRAND_INT32
psrand_seeded_noise(PSRAND_INT32 _x, PSRAND_INT32 _seed);


PSRAND_INT32
psrand_seeded_noise_2d(PSRAND_INT32 _x, PSRAND_INT32 _y, PSRAND_INT32 _seed);

PSRAND_INT32
psrand_seeded_noise_3d(PSRAND_INT32 _x, PSRAND_INT32 _y, PSRAND_INT32 _z, PSRAND_INT32 _seed);

#endif /*PSRAND_H*/
/******************************************************************************/
#ifdef PSRAND_IMPLEMENTATION

#ifndef PSRAND_USE_CUSTOM_VALUES
	static const PSRAND_INT32U seeded_pos_x = 0xBD4BCB5;
	static const PSRAND_INT32U seeded_pos_y = 0x63D309;
	static const PSRAND_INT32U mangle_1     = 0x3D73;
	static const PSRAND_INT32U mangle_2     = 0xC0AE5;
	static const PSRAND_INT32U mangle_3     = 0x5208DD0D;
	static const PSRAND_INT32U mangle_shift = 13;
	static const PSRAND_INT32U noise_1      = 0xB5297A4D;
	static const PSRAND_INT32U noise_2      = 0x68E31DA4;
	static const PSRAND_INT32U noise_3      = 0x1B56C4E9;
#endif /*PSRAND_USE_CUSTOM_VALUES*/

uint32_t
psrand_seeded_ulong(uint32_t _seed)
/**
 * psrand_seeded_ulong() - generate seeded random uint32.
 * @arg1: the seed used to generate random value.
 *
 * A longer description, with more discussion of the function function_name()
 * that might be useful to those using or modifying it. Begins with an
 * empty comment line, and may include additional embedded empty
 * comment lines.
 *
 * The longer description may have multiple paragraphs.
 *
 * Context: Describes whether the function can sleep, what locks it takes,
 *          releases, or expects to be held. It can extend over multiple
 *          lines.
 * Return: Describe the return value of function_name.
 *
 * The return value description can also have multiple paragraphs, and should
 * be placed at the end of the comment block.
 */
{
	uint32_t n;
	n = (_seed << mangle_shift) ^ _seed;
	n *= n * mangle_1;
	n += mangle_2;
	n *= n;
	n += mangle_3;
	return n;
}

uint8_t
psrand_seeded_bool(uint32_t _seed)
{
	return (psrand_seeded_ulong(_seed) & 1);
}

uint32_t
psrand_seeded_ubetween(uint32_t _min, uint32_t _max, uint32_t _seed)
{
	uint32_t v = psrand_seeded_ulong(_seed);
	return ((v % (_max - _min + 1)) + _min);
}

uint32_t
psrand_random_ulong(void)
{
	return psrand_seeded_ulong(PSRAND_AUTO_SEED_FUNCTION);
}

uint8_t
psrand_random_bool(void) 
{ 
	return psrand_seeded_bool(PSRAND_AUTO_SEED_FUNCTION); 
}

uint32_t
psrand_random_ubetween(uint32_t _min, uint32_t _max)
{
	return psrand_seeded_ubetween(_min, _max, PSRAND_AUTO_SEED_FUNCTION);
}

int32_t
psrand_seeded_noise(int32_t _x, int32_t _seed)
{
	int32_t n;
	n  = _x;
	n *= noise_1;
	n += _seed;
	n ^= (n >> 8);
	n *= noise_2;
	n ^= (n << 8);
	n *= noise_3;
	n ^= (n >> 8);
	return n; 
}

int32_t
psrand_seeded_noise_2d(int32_t _x, int32_t _y, int32_t _seed)
{
	return psrand_seeded_noise(_x + (seeded_pos_x * _y), _seed);
}

int32_t
psrand_seeded_noise_3d(int32_t _x, int32_t _y, int32_t _z, int32_t _seed)
{
	int32_t v = _x + (seeded_pos_x * _y) + (seeded_pos_y * _z);
	return psrand_seeded_noise(v, _seed);
}


/*TODO: Add proper function descriptions*/
/**
 * function_name() - Brief description of function.
 * @arg1: Describe the first argument.
 * @arg2: Describe the second argument.
 *        One can provide multiple line descriptions
 *        for arguments.
 *
 * A longer description, with more discussion of the function function_name()
 * that might be useful to those using or modifying it. Begins with an
 * empty comment line, and may include additional embedded empty
 * comment lines.
 *
 * The longer description may have multiple paragraphs.
 *
 * Context: Describes whether the function can sleep, what locks it takes,
 *          releases, or expects to be held. It can extend over multiple
 *          lines.
 * Return: Describe the return value of function_name.
 *
 * The return value description can also have multiple paragraphs, and should
 * be placed at the end of the comment block.
 */

#endif /*PSRAND_IMPLEMENTATION*/

#ifdef __cplusplus
}
#endif
