#include <stdio.h>

#include "test_conv_library.h"

#define CONV_CALL(call)                                                        \
    do {                                                                       \
        conv_clear_errors();                                                   \
        call                                                                   \
        parse_conv_errors(stdout, (char*)"CONV", (char*)__FILE__, __LINE__);   \
    } while(0);


void
parse_conv_errors(FILE* _stream, char* _type, char* _file_, int _line_)
{
	ERROR_FLAGGER_ERROR_TYPE err;
	while ((err = conv_get_error()) != ERROR_FLAGGER_NO_ERROR)
		fprintf(_stream,
		        "[%s ERROR] -> code(%d) {%s, %d}\n",
		        _type, err, _file_, _line_);
}

int main(int argc, char **argv) {
	(void)argc;
	(void)argv;
	
	unsigned char t;
	CONV_CALL(t = conv_function(45);)
	printf("val = (%d)\n", t);

	CONV_CALL(t = conv_function(329);)
	printf("val = (%d)\n", t);

	return 0;
}
