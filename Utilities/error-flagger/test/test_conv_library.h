/*A test library for showing error_flagger.h test functionality
 * */

#ifndef ERROR_FLAGGER_IMPLEMENTATION
#define ERROR_FLAGGER_IMPLEMENTATION
#endif /*ERROR_FLAGGER_IMPLEMENTATION*/
#include "../error_flagger.h"

#ifndef TEST_SOMELIBRARY_H
#define TEST_SOMELIBRARY_H

/* Error code enums for the error manager.
 * */
enum CONV_ERROR_FLAGS {
	CONV_INVALID_SIZE_OF_INPUT = 4,
};

/* A Global "static" error manager is defined for error management in the
 * library.
 * */
static struct error_flagger_manager _conv_ef = {0};

/* 3 functions that manage the internal error_flagger_manager 
 * (hides the manager) and allows it to be static.
 * */
void
conv_clear_errors(void) { _error_flagger_clear_errors(&_conv_ef); }
void
conv_put_error(char _err) { _error_flagger_put_error(&_conv_ef, _err); }
char
conv_get_error(void) { return _error_flagger_get_error(&_conv_ef); }

/* A function where a undefined error could occour is defined.
 * */
unsigned char
conv_function(int _v)
/* some_function() -> cast a int to a char. 
 *
 * Return: If a int larget than max_char or less than 0 is entered, throw a
 *         error flag to error manager, instead of asserting.
 * */
{
	if (_v > 255 || _v < 0)
		conv_put_error(CONV_INVALID_SIZE_OF_INPUT);
	return _v;
}

#endif /*TEST_SOMELIBRARY_H*/
