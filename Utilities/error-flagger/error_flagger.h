/* 
SHORT DESCRIPTION:
	error_flagger.h - Easy to use soft error management tool 
	written in ansi-c99.

ORIGINAL AUTHOR:
	Thomas Alexgaard Jensen (https://gitlab.com/Alexgaard)

   __________________________
  :##########################:
  *##########################*
           .:######:.
          .:########:.
         .:##########:.
        .:##^:####:^##:.
       .:##  :####:  ##:.
      .:##   :####:   ##:.
     .:##    :####:    ##:.
    .:##     :####:     ##:.
   .:##      :####:      ##:.
  .:#/      .:####:.      \#:. 


LICENSE:
	Copyright (c) <Thomas Alexgaard Jensen>
	This software is provided 'as-is', without any express or implied warranty.
	In no event will the authors be held liable for any damages arising from
	the use of this software.
	Permission is granted to anyone to use this software for any purpose,
	including commercial applications, and to alter it and redistribute it
	freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented; you must not
	   claim that you wrote the original software. If you use this software in
	   a product, an acknowledgment in the product documentation would be
	   appreciated but is not required.

	2. Altered source versions must be plainly marked as such, and must not be
	   misrepresented as being the original software.

	3. This notice may not be removed or altered from any source distribution.

	For more information, please refer to 
	<https://choosealicense.com/licenses/zlib/>

DESCRIPTION:
	A simple to use, and barebones error management tool inspired by the OpenGL
	<glGetError()> function.

	error_flagger.h is an unintrusive alternative to context-breaking asserts
	in library code.

	The tool can simply be included and used in any library.

CHANGELOG:
	[0.2] Rewritten to header-only-library.
	      Updated cookbook.
	      Updated Documentation.
	[0.1] Implemented core utilities.
	[0.0] Initialized library.

DOCUMENTATION:
	To use library, define ERROR_FLAGGER_IMPLEMENTATION before include.

	The tool is designed to work around the idea of non-intrusive & voulentary
	error management and checking. The idea is that the library designer, 
	defines a set of error codes that the library is able to emit at runtime 
	without explicit assertion, and then it is up to the user of that library
	to make use of those emitted errors.

	The library interprets <NO ERROR = 0>. therefore 0 cannot be an 
	active error!

	WHY USE THIS LIBRARY?

	direct assertions are liable to directly mess up code context when running
	code that executes for a long time. what do you do if a assertions happens
	after executing for an hour without serialization?
	On top of this assertions are stripped when releasing code anyway, they only
	serve a purpose for debugging.

	Directly printing to stderr can be much more coumbersome and much more 
	isolated as they are only exposed at code execution stops.

COOKBOOK:

LIBRARY EXAMPLE IMPLEMENTATION

When implementing error flagger into a library, there are some steps that are
recommended:

1) Define a global <struct error_flagger_manager>.
2) Define a error enums.
3) Encase <_error_flagger_*> functions in library specific error functions.
4) Implement error pushing when errors occour.

The entire code setup looks like this (Note that a imaginary library 
called <conv> is used for the example):

enum CONV_ERROR_FLAGS {
	CONV_INVALID_SIZE_OF_INPUT = 4,
};

static struct error_flagger_manager _conv_ef = {0};

void
conv_clear_errors(void) { _error_flagger_clear_errors(&_conv_ef); }
void
conv_put_error(char _err) { _error_flagger_put_error(&_conv_ef, _err); }
char
conv_get_error(void) { return _error_flagger_get_error(&_conv_ef); }

unsigned char
conv_function(int _v)
//some_function() -> cast a int to a char. 
//
//Return: If a int larget than max_char or less than 0 is entered, throw a
//        error flag to error manager, instead of asserting.
{
	if (_v > 255 || _v < 0)
		conv_put_error(CONV_INVALID_SIZE_OF_INPUT);
	return _v;
}

The specific thing to note is that the library specific functions encase the
static error_flagger_manager. The library errors themselves are also propagated
using the library specific error functions.

USER OF LIBRARY EXAMPLE IMPLEMENTATION

To make use of the library specific error functions in a "smart" way, we need to
define a function that is able to descriptively parse the error codes.
Then dynamically checking for errors when using the library comes down to:

    clearing -> calling -> checking

This could be wrapped in a macro, for simplicity:

#include "test_conv_library.h"

#define CONV_CALL(call)                                                        \
    do {                                                                       \
        conv_clear_errors();                                                   \
        call                                                                   \
        parse_conv_errors(stdout, (char*)"CONV", (char*)__FILE__, __LINE__);   \
    } while(0);


void
parse_conv_errors(FILE* _stream, char* _type, char* _file_, int _line_)
{
	ERROR_FLAGGER_ERROR_TYPE err;
	while ((err = conv_get_error()) != ERROR_FLAGGER_NO_ERROR)
		fprintf(_stream,
		        "[%s ERROR] -> code(%d) {%s, %d}\n",
		        _type, err, _file_, _line_);
}


	unsigned char t = conv_function(37);
	printf("val = (%d)\n", t);

	CONV_CALL(t = conv_function(329);)
	printf("val = (%d)\n", t);

The important thing to note here is that we wrap the error checking function in
a easy-to-use checker macro, that will print any errors that might occour.

To avoid overhead of release builds, the macro could be further expanded:

#ifdef RELEASE
#define CONV_CALL(call)                                                        \
    call
#else
#define CONV_CALL(call)                                                        \
    do {                                                                       \
        conv_clear_errors();                                                   \
        call                                                                   \
        parse_conv_errors(stdout, (char*)"CONV", (char*)__FILE__, __LINE__);   \
    } while(0);
#endif

This excludes all error overhead alltogether.

*/

#ifdef __cplusplus
extern "C" {
#endif

#ifndef ERROR_FLAGGER_H
#define ERROR_FLAGGER_H

#ifndef ERROR_FLAGGER_SIZE
#define ERROR_FLAGGER_SIZE 8
#endif /*error_flagger_manager_SIZE*/

#ifndef ERROR_FLAGGER_ERROR_TYPE
#define ERROR_FLAGGER_ERROR_TYPE char
#endif /*ERROR_FLAGGER_ERROR_TYPE*/

#define ERROR_FLAGGER_NO_ERROR 0

struct error_flagger_manager {
	ERROR_FLAGGER_ERROR_TYPE errors[ERROR_FLAGGER_SIZE];
	unsigned int idx;
};

ERROR_FLAGGER_ERROR_TYPE
_error_flagger_get_error(struct error_flagger_manager* _ef);

unsigned int
_error_flagger_put_error(struct error_flagger_manager* _ef,
                         ERROR_FLAGGER_ERROR_TYPE _err);

void
_error_flagger_clear_errors(struct error_flagger_manager* _ef);


#endif /*ERROR_FLAGGER_H*/
/******************************************************************************/
#ifdef ERROR_FLAGGER_IMPLEMENTATION

ERROR_FLAGGER_ERROR_TYPE
_error_flagger_get_error(struct error_flagger_manager* _ef)
{
	if (_ef->idx > 0)
		return _ef->errors[--_ef->idx];
	return ERROR_FLAGGER_NO_ERROR;
}

unsigned int
_error_flagger_put_error(struct error_flagger_manager* _ef,
                         ERROR_FLAGGER_ERROR_TYPE _err)
{
	/*TODO: Relocate all errors down, removing the first, then place.
	 *      Currently just loops around and overwrites*/
	if (_ef->idx > ERROR_FLAGGER_SIZE)
		_ef->idx = 0;

	_ef->errors[_ef->idx++] = _err;
	return _ef->idx;
}

void
_error_flagger_clear_errors(struct error_flagger_manager* _ef)
{
	while (_ef->idx > 0)
		_error_flagger_get_error(_ef);
}

#endif /*ERROR_FLAGGER_IMPLEMENTATION*/

#ifdef __cplusplus
}
#endif
