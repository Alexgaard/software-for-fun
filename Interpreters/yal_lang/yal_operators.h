#ifndef YAL_LANG_OPERATORS_H
#define YAL_LANG_OPERATORS_H

#include "yal_lang.h"

yal_atom*
YALOP_WRITE(yal_enviroment* _env, yal_atom* _list, int _thisidx)
{
    (void)_env;
    if (_list == NULL)
        YAL_ASSERT("invalid atom input ptr");

    if (_list->type != YAL_LIST)
        YAL_ASSERT("invalid atom input");
    if (yal_atomlist_len(&_list->list) < 1)
        YAL_ASSERT("invalid write input, nothing to print");

    printf("calling YALOP_WRITE\n");

    /*relocate members to remove function write*/
    yal_atomlist_remove(&_list->list, _thisidx);
    /*Print entry*/
    yal_atomlist_print(&_list->list);
    return _list;
}

yal_atom*
YALOP_PLUS(yal_enviroment* _env, yal_atom* _list, int _thisidx)
{
    (void)_env;

    if (_list->type != YAL_INT)
        YAL_ASSERT("invalid atom input");
    if (yal_atomlist_len(&_list->list) < 3)
        YAL_ASSERT("invalid int input, nothing to concatenate");

    printf("calling YALOP_PLUS\n");

    int val = 0;
    int i;
    for (i = _thisidx + 1; i < yal_atomlist_len(&_list->list); i++) {
        yal_atom* tmp = *yal_atomlist_peek(&_list->list, i);
        YAL_DEBUG3("adding %d to %d\n", tmp->value_int, val);
        val += tmp->value_int;
        yal_atomlist_remove(&_list->list, i);
    }
    return yal_atom_int((char*)"+res", val);
}
/*
yal_atom*
YALOP_MINUS(yal_enviroment* _env, yal_atom* _list)
{
    (void)_env;
    yal_atom* it = _list->next;
    int val = 0;
    val = it->value_int;
    it = it->next;
    while (it != NULL) {
        if (it->type != YAL_INT)
            YAL_ASSERT("invalid plus input");
        val -= it->value_int;
        it = it->next;
    }
    return yal_atom_int(NULL, val);
}

yal_atom*
YALOP_MULTIPLY(yal_enviroment* _env, yal_atom* _list)
{
    (void)_env;
    yal_atom* it = _list->next;
    int val = 0;
    val = it->value_int;
    it = it->next;
    while (it != NULL) {
        if (it->type != YAL_INT)
            YAL_ASSERT("invalid multiply input");
        val *= it->value_int;
        it = it->next;
    }
    return yal_atom_int(NULL, val);
}

yal_atom*
YALOP_DIVIDE(yal_enviroment* _env, yal_atom* _list)
{
    (void)_env;
    yal_atom* it = _list->next;
    int val = 0;
    val = it->value_int;
    it = it->next;
    while (it != NULL) {
        if (it->type != YAL_INT)
            YAL_ASSERT("invalid multiply input");
        val /= it->value_int;
        it = it->next;
    }
    return yal_atom_int(NULL, val);
}
 * */

#endif /*YAL_LANG_OPERATORS_H*/
