void
yal_atom_print_info(yal_atom* _atom)
{
    char* type = NULL;
    char* has_next = NULL;
    if (_atom == NULL)
        return;
    switch (_atom->type) {
    case YAL_FUNCTION:
        type = (char*)"{function}";
        break;
    case YAL_INT:
        type = (char*)"{int}";
        break;
    default:
        type = (char*)"{null}";
        break;
    }
    if (_atom->sym_type != YAL_NOT_SYM) {
        type = (char*)"{sym}";
    }
    has_next = (_atom->next != NULL) ? (char*)"Y" : (char*)"N";
    printf("type = %s, symbol = %s, next? = %s, value = ", type, _atom->name, has_next);
    yal_atom_print(_atom);
    printf("\n");
}



void
yal_simple_evaluate(yal_atom* _root)
{
    yal_atom* tmp = NULL;
    if (_root->nested != NULL) {
        yal_simple_evaluate(_root->nested);
        /*manually insert nested expression after evaluation*/
        tmp = _root->next;
        _root->next = _root->nested;
        _root->next->next = tmp;
        _root->nested = NULL;
    }
    if (_root->next != NULL) {
        yal_simple_evaluate(_root->next);
    }
    if (_root->type == YAL_FUNCTION) {
        *_root = _root->function(*_root);
    }
    YAL_DEBUG1("-> nothing to evaluate\n");
}


