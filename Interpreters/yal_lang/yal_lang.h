/*
 * Yal atom structure:
 *
 * (write (+ 2 (* 4 3)))
 *
 * [{list}]
 *  |
 *  [{sym:write} | {list}]
 *                 |
 *                 [{sym:+} | 2 | {list}]
 *                                |
 *                                [{sym:*} | 2 | 3]
 * */

#ifndef YAL_LANG_H
#define YAL_LANG_H

#include "stdio.h"
#include "stdlib.h"
#include "assert.h"
#include "stdarg.h"

#define YAL_ANONYMOUS (char*)"{anon}"

#ifndef YAL_NO_ASSERT
#define YAL_ASSERT(expr) assert(expr)
#else 
#define YAL_ASSERT(expr)
#endif

#ifndef YAL_DEBUG_LEVEL
#define YAL_DEBUG_LEVEL 0
#endif

#if YAL_DEBUG_LEVEL == 0
    #define YAL_DEBUG1(str, ...)
    #define YAL_DEBUG2(str, ...)
    #define YAL_DEBUG3(str, ...)
#elif YAL_DEBUG_LEVEL == 1 
    #define YAL_DEBUG1(str, ...) printf(str, ##__VA_ARGS__)
    #define YAL_DEBUG2(str, ...)
    #define YAL_DEBUG3(str, ...)
#elif YAL_DEBUG_LEVEL == 2
    #define YAL_DEBUG1(str, ...) printf(str, ##__VA_ARGS__)
    #define YAL_DEBUG2(str, ...) printf(str, ##__VA_ARGS__)
    #define YAL_DEBUG3(str, ...)
#elif YAL_DEBUG_LEVEL == 3
    #define YAL_DEBUG1(str, ...) printf(str, ##__VA_ARGS__)
    #define YAL_DEBUG2(str, ...) printf(str, ##__VA_ARGS__)
    #define YAL_DEBUG3(str, ...) printf(str, ##__VA_ARGS__)
#endif

enum YAL_ATOM_TYPE {
    YAL_LIST = 0,
    YAL_INT,
    YAL_FUNCTION,

    YAL_TYPE_COUNT
};

typedef struct _yal_atom yal_atom;
typedef yal_atom* yal_atom_ptr;


#define TSARRAY_NAME_OVERRIDE
#define t_type yal_atom_ptr
#define arr_t_name yal_atomlist
yal_atom*
yal_atom_destroy(yal_atom* _atom);
#define t_operator_destroy(t) yal_atom_destroy(t)
#include "vendor/tsarray.h"

typedef struct _yal_enviroment yal_enviroment;

typedef yal_atom* (*yal_function_ptr)(yal_enviroment* _env, yal_atom* _list, int _thisidx);

struct _yal_atom {
    char* name;
    char is_sym;
    enum YAL_ATOM_TYPE type;
    union {
        int value_int; 
        yal_function_ptr function;
        yal_atomlist list;
    };
};

struct _yal_enviroment {
    yal_atomlist symbols;
};

yal_atom*
yal_atom_new(void);

yal_atom*
yal_atom_int(char* _name, int _v);

yal_atom*
yal_atom_function(char* _name, yal_function_ptr _fn);

yal_atom*
yal_symbol(char* _name);

yal_atom*
yal_symbol_register(yal_enviroment* _env, yal_atom* _symbol);

void
yal_atom_print(yal_atom* _atom);

void
yal_list_print(yal_atom* _list);

void
yal_simple_evaluate(yal_atom* _atom);

void
yal_evaluate(yal_enviroment* _env, yal_atom* _atom);

//#ifdef YAL_LANG_IMPLEMENTATION

char
_yal_str_compare(char* _a, char* _b)
{
    int i = 0;
    if (_a == NULL || _b == NULL)
        return 0;
    while (_a[i] == _b[i]) {
        if (_a[i] == '\0' || _b[i] == '\0')
            return 1;
        i++;
    }
    return 0;
}

yal_atom*
_yal_find_symbol(yal_enviroment* _env, yal_atom* _sym)
{
    int i;
    char res = 0;
    if (_env == NULL || _env->symbols.members == NULL || _sym == NULL)
        return NULL;
    for (i = 0; i < yal_atomlist_len(&_env->symbols); i++) {
        yal_atom* ptr = *yal_atomlist_peek(&_env->symbols, i);
        res =_yal_str_compare(_sym->name, ptr->name); 
        //YAL_DEBUG3("Comparing [%s] & [%s] ?= %d\n", _sym->name, _env->symbols[i].name, res);
        if (res)
            return *yal_atomlist_peek(&_env->symbols, i);
    }
    assert("Unreachable, no symbol matched!");
    return NULL;
}

yal_atom*
yal_atom_destroy(yal_atom* _atom)
{
    if (_atom != NULL)
        free(_atom);
    return NULL;
}

yal_atom*
yal_atom_new(void)
{
    yal_atom* out = (yal_atom*)malloc(sizeof(yal_atom));
    if (out == NULL)
        return NULL;
    *out = (yal_atom){0};
    out->is_sym = 0;
    out->type = YAL_LIST;
    return out;
}

yal_atom*
yal_atom_int(char* _name, int _v)
{
    yal_atom* out = yal_atom_new();
    if (out == NULL)
        return NULL;
    out->type = YAL_INT;
    if (_name == NULL)
        out->name = YAL_ANONYMOUS;
    else
        out->name = _name;
    out->value_int = _v;
    return out;
}

yal_atom*
yal_atom_function(char* _name, yal_function_ptr _fn)
{
    yal_atom* out = yal_atom_new();
    if (out == NULL)
        return NULL;
    out->type = YAL_FUNCTION;
    out->name = _name;
    out->function = _fn;
    return out;
}

yal_atom*
yal_symbol(char* _name)
{
    yal_atom* out = NULL;
    if (_name == NULL)
        return NULL;
    out = yal_atom_new();
    if (out == NULL)
        return NULL;
    *out = (yal_atom) {0};
    out->is_sym = 1;
    out->name = _name;
    return out;
}

yal_atom*
yal_symbol_register(yal_enviroment* _env, yal_atom* _symbol)
{
    if (_env == NULL || _symbol == NULL)
        return NULL;

    _symbol->is_sym = 1;
    yal_atomlist_push(&_env->symbols, _symbol);
    yal_atom_destroy(_symbol);
    return *yal_atomlist_peek(&_env->symbols, -1);
}

void
yal_atom_print(yal_atom* _atom)
{
    if (_atom == NULL)
        return;
    if (_atom->is_sym) {
        printf("{sym:%s}", _atom->name);
        return;
    }
    switch (_atom->type) {
    case YAL_INT:
        printf("%d", _atom->value_int);
        break;
    case YAL_FUNCTION:
       printf("%s", _atom->name);
        break;
    case YAL_LIST:
        printf("{list}");
        break;
    default:
        printf("{print=impossible}");
        break;
    }
}

void
yal_list_print(yal_atom* _list)
{
    int i;
    if (_list->type == YAL_LIST) {
        printf("( ");
        for (i = 0; i < yal_atomlist_len(&_list->list); i++)
            yal_list_print(*yal_atomlist_peek(&_list->list, i));
        printf(") ");
        return;
    }
    else {
        yal_atom_print(_list);
        printf(" ");
    }
}

void
yal_evaluate(yal_enviroment* _env, yal_atom* _root)
{
    //YAL_DEBUG2("evaluating token [%s]\n\t", _root->name);
    //if (YAL_DEBUG_LEVEL == 3) yal_atom_print_info(_root);
    if (_root == NULL)
        return;

    int i;
    int j;
    yal_atom* curr = NULL;
    /*Check for symbols*/
    for (i = 0; i < yal_atomlist_len(&_root->list); i++) {
        curr = *yal_atomlist_peek(&_root->list, i);
        /*Handle symbols*/
        if (curr->is_sym) {
            YAL_DEBUG3("-> evaluating symbol %s\n", _root->name);
            yal_atom* sym = _yal_find_symbol(_env, _root);
            *_root = *sym;
        }
        /*Handle functions*/
        if (curr->type == YAL_FUNCTION) {
            YAL_DEBUG3("-> evaluating direct function %s\n", _root->name);
            _root = _root->function(_env, _root, i);
        }
        /*Handle lists*/
        if (curr->type == YAL_LIST) {
            YAL_DEBUG3("-> evaluating list\n");
            for (j = 0; j < yal_atomlist_len(&curr->list); j++)
                yal_evaluate(_env, *yal_atomlist_peek(&curr->list, j));
        }
        YAL_DEBUG1("-> nothing to evaluate for symbol [%s]\n", _root->name);
    }

}
//#endif /*YAL_LANG_IMPLEMENTATION*/

#endif /*YAL_LANG_H*/
