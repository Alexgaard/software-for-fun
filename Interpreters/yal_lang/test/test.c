#include <stdio.h>
#include <assert.h>

#define YAL_LANG_IMPLEMENTATION
#define YAL_DEBUG_LEVEL 3
#include "../yal_lang.h"
#include "../yal_operators.h"


void
yal_env_defaults(yal_enviroment* _env)
{
    yal_symbol_register(_env, yal_atom_function("write", YALOP_WRITE));
    yal_symbol_register(_env, yal_atom_function("+",     YALOP_PLUS));
//   yal_symbol_register(_env, yal_atom_function("-",     YALOP_MINUS));
//   yal_symbol_register(_env, yal_atom_function("*",     YALOP_MULTIPLY));
//   yal_symbol_register(_env, yal_atom_function("/",     YALOP_DIVIDE));
}

void
test_enviroment(void)
{
    yal_enviroment env = {0};    
    yal_env_defaults(&env);

    yal_atom* s_write = yal_symbol("write");
    yal_atom* s_mult = yal_symbol("+");

    yal_atom* a = yal_atom_int("a", 7);
    yal_atom* b = yal_atom_int("b", 3);

    /* Emulate code:
     * (write (+ a b))
     * */
    yal_atom* root = yal_atom_new();
    yal_atomlist_push(&root->list, s_write);
    yal_atomlist_push(&root->list, yal_atom_new());

    printf("root contains %d elems\n", yal_atomlist_len(&root->list));

    root->list.members[1]->type = YAL_LIST;
    yal_atomlist_push(&root->list.members[1]->list, s_mult);
    yal_atomlist_push(&root->list.members[1]->list, a);
    yal_atomlist_push(&root->list.members[1]->list, b);

    printf("root->child_1 contains %d elems\n", yal_atomlist_len(&root->list.members[1]->list));


    yal_list_print(root);
    printf("\n");
    yal_evaluate(&env, root);

    printf("\n");
}

int main(void) {
    test_enviroment();
    return 0;
}
