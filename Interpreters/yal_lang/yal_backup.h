#ifndef YAL_LANG_H
#define YAL_LANG_H

#include "stdio.h"
#include "stdlib.h"

#define YAL_ANONYMOUS "{anon}"

enum YAL_TYPE {
    YAL_NULL = 0,
    YAL_INT,
    YAL_STRING,
    YAL_LIST,
    YAL_FUNCTION,

    YAL_TYPE_COUNT
};

struct _yal_list;
typedef struct _yal_list yal_list;
typedef struct _yal_atom yal_atom;
typedef yal_list (*yal_function)(yal_list _list);

struct _yal_atom {
    char* name;
    char is_symbol;
    char type;
    union {
        int value_int; 
        char* value_string; 
        yal_function function;
        yal_list* list;
    };
};

struct _yal_list {
    int size;
    int max;
    yal_atom* members;
};

yal_list
yal_list_n(int _n);

yal_atom
yal_atom_null(void);

yal_atom
yal_atom_int(char* _name, int _v);

yal_atom
yal_atom_function(char* _name, yal_function _fn);

void
yal_atom_write(yal_atom _atom);

void
yal_list_write(yal_list _list);

yal_list
yal_evaluate_atom_list(yal_list _list);

#ifdef YAL_LANG_IMPLEMENTATION

yal_list
yal_list_n(int _n)
{
    yal_list out = {0};
    out.members = (yal_atom*)malloc(sizeof(yal_atom) * _n);
    if (out.members == NULL)
        return out;
    out.max = _n;
    out.size = 0;
    return out;
}

yal_atom
yal_atom_null(void)
{
    yal_atom out = {0};
    out.type = YAL_NULL;
    return out;
}

yal_atom
yal_symbol(yal_atom _init)
{
    _init.is_symbol = 1;
    return _init;
}

yal_atom
yal_atom_list(yal_list* _list)
{
    yal_atom out = {0};
    out.type = YAL_LIST;
    out.list = _list;
    return out;
}

yal_list*
yal_list_push(yal_list* _list, yal_atom _atom)
{
    if (_list == NULL || _list->size == _list->max)
        return NULL;
    _list->members[_list->size++] = _atom;
    return _list;
}

yal_atom
yal_atom_int(char* _name, int _v)
{
    yal_atom out = {0};
    out.type = YAL_INT;
    if (_name == NULL)
        out.name = YAL_ANONYMOUS;
    else
    out.name = _name;
    out.value_int = _v;
    return out;
}

yal_atom
yal_atom_function(char* _name, yal_function _fn)
{
    yal_atom out = {0};
    out.type = YAL_FUNCTION;
    out.name = _name;
    out.function = _fn;
    return out;
}

void
yal_atom_write(yal_atom _atom)
{
    switch (_atom.type) {
    case YAL_INT:
        printf("%d", _atom.value_int);
        break;
    case YAL_STRING:
        printf("%s", _atom.value_string);
        break;
    case YAL_FUNCTION:
       printf("%s", _atom.name);
        break;
    case YAL_NULL:
        printf("{null}");
        break;
    case YAL_LIST:
        printf("{list}");
        break;
    default:
        printf("{impossible}");
        break;
    }
}

void
_yal_list_print_recursive(yal_list _list)
{
    int i;
    printf("( ");
    for (i = 0; i < _list.size; i++) {
        if (_list.members[i].type == YAL_LIST) {
            _yal_list_print_recursive(*_list.members[i].list);
        } 
        else {
            yal_atom_write(_list.members[i]);
            printf(" ");
        }
    }
    printf(")");
}

void
yal_list_print(yal_list _list)
{
    _yal_list_print_recursive(_list);
    printf("\n");
}

yal_list
yal_evaluate_atom_list(yal_list _list)
{
    int i;
    yal_list tmp = {0};
    for (i = 0; i < _list.size; i++) {
        //printf("evaluating token [%s]\n", _list.members[i].name);
        switch(_list.members[i].type) {
        case YAL_LIST:
            tmp = yal_evaluate_atom_list(*_list.members[i].list);
            _list.members[i] = yal_atom_list(&tmp);
            break;
        case YAL_FUNCTION:
            return _list.members[i].function(_list);
        default:
            printf("evaluating token is impossible\n");
            break;
        }
    }
    return _list;
}

#endif /*YAL_LANG_IMPLEMENTATION*/

#endif /*YAL_LANG_H*/
